import delivery from './delivery.png';
import pickup from './pickup.png';

export default { pickup, delivery };
