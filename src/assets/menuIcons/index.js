import FavoriteLocation from './FavoriteLocation.png';
import Invoices from './Invoices.png';
import OrderHistory from './OrderHistory.png';
import PlaceOrder from './PlaceOrder.png';
import Templates from './Templates.png';
import Settings from './Settings.png';

export default {
  'Favorite Location': FavoriteLocation,
  Invoices,
  'Order History': OrderHistory,
  'Place Order': PlaceOrder,
  Templates,
  Settings,
};
