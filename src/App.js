import React from 'react';
import { GlobalStyles } from './fonts/fonts';
import TopNav from './components/TopNav';
import SideMenu from './components/SideMenu';
import MainContent from './components/MainContent';
import { AppContainer, ContentContainer } from './styles/AppStyles';

function App() {
  return (
    <>
      <GlobalStyles />
      <AppContainer>
        <TopNav />
        <ContentContainer>
          <SideMenu />
          <MainContent />
        </ContentContainer>
      </AppContainer>
    </>
  );
}

export default App;
