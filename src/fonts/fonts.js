import { createGlobalStyle } from 'styled-components';

import Inter from './Inter/Inter-VariableFont_slnt,wght.ttf';

export const GlobalStyles = createGlobalStyle`
  @font-face {
    font-family: Inter;
    font-style: normal;
    font-weight: 400;
    src: url(${Inter});
  }
`;
