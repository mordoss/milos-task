import React from 'react';
import {
  SideMenuContainer,
  Tabs,
  Tab,
  Icon,
  TabText,
  Settings,
  Avatar,
} from '../styles/SideMenuStyles';

const tabs = [
  'Place Order',
  'Favorite Location',
  'Order History',
  'Templates',
  'Invoices',
];

const SideMenu = () => {
  return (
    <SideMenuContainer>
      <Tabs>
        {tabs.map((tab, index) => (
          <Tab key={index} active={tab === 'Place Order'}>
            <Icon icon={tab} />
            <TabText> {tab}</TabText>
          </Tab>
        ))}
      </Tabs>
      <Settings>
        <Avatar />
        <Icon settings icon="Settings" />
        <TabText> Settings </TabText>
      </Settings>
    </SideMenuContainer>
  );
};

export default SideMenu;
