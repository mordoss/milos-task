import React from 'react';
import {
  TopNavContainer,
  LogoContainer,
  Logo,
  LogoText,
  Input,
} from '../styles/TopNavStyles';

const TopNav = () => {
  return (
    <TopNavContainer>
      <LogoContainer>
        <Logo />
        <LogoText />
      </LogoContainer>

      <Input />
    </TopNavContainer>
  );
};

export default TopNav;
