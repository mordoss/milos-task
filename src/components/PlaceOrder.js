import React from 'react';
import PlaceOrderAction from './placeOrderComponents/PlaceOrderAction';
import DeliveryInfo from './placeOrderComponents/DeliveryInfo';
import {
  PlaceOrderContainer,
  InfoContainer,
  InfoItem,
  InfoNumber,
  InfoText,
} from '../styles/PlaceOrderStyles';

const infoItems = [
  ['Assigned', 3, 'orange'],
  ['Started', 23, 'blue'],
  ['Successfull', 46, 'green'],
  ['Failed', 1, 'pink'],
  ['Driver Accepted', 21, 'purple'],
  ['Returned', 2, 'red'],
];

const PlaceOrder = () => {
  return (
    <PlaceOrderContainer>
      <InfoContainer>
        {infoItems.map((item, index) => (
          <InfoItem key={index} color={item[2]}>
            <InfoNumber>{item[1]}</InfoNumber>
            <InfoText>{item[0]}</InfoText>
          </InfoItem>
        ))}
      </InfoContainer>
      <DeliveryInfo />
      <PlaceOrderAction />
    </PlaceOrderContainer>
  );
};

export default PlaceOrder;
