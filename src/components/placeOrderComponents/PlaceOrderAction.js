import React from 'react';
import {
  PlaceOrderActionContainer,
  PlaceOrderTitle,
  Details,
  DetailsIcon,
  DetailsText,
  DetailsAdd,
  DetailsButton,
  DetailsButtonText,
} from '../../styles/PlaceOrderStyles';

const PlaceOrderAction = () => {
  return (
    <PlaceOrderActionContainer>
      <PlaceOrderTitle>Place Order</PlaceOrderTitle>
      <>
        <Details>
          <DetailsIcon icon="pickup" />
          <DetailsText>Pickup Details</DetailsText>
          <DetailsAdd>Add</DetailsAdd>
        </Details>
        <Details>
          <DetailsIcon icon="delivery" />
          <DetailsText>Delivery Details</DetailsText>
          <DetailsAdd>Add</DetailsAdd>
        </Details>
      </>
      <DetailsButton>
        <DetailsButtonText>Place Order</DetailsButtonText>
      </DetailsButton>
    </PlaceOrderActionContainer>
  );
};

export default PlaceOrderAction;
