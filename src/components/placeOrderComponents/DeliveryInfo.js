import React from 'react';
import {
  InfoNumber,
  DeliveryInfoContainer,
  DeliveryInfoItem,
  InfoDeliveryText,
} from '../../styles/PlaceOrderStyles';

const PlaceOrder = () => {
  return (
    <DeliveryInfoContainer>
      <DeliveryInfoItem dark>
        <InfoNumber>12</InfoNumber>
        <InfoDeliveryText>Redelivered</InfoDeliveryText>
      </DeliveryInfoItem>
      <DeliveryInfoItem>
        <InfoNumber>5</InfoNumber>
        <InfoDeliveryText>Unassigned</InfoDeliveryText>
      </DeliveryInfoItem>
    </DeliveryInfoContainer>
  );
};

export default PlaceOrder;
