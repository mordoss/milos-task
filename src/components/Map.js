import React from 'react';
import { MapImage, ColorBlend } from '../styles/MapStyles';

const Map = () => {
  return (
    <MapImage>
      <ColorBlend />
    </MapImage>
  );
};

export default Map;
