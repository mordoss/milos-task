import React from 'react';
import Map from './Map';
import PlaceOrder from './PlaceOrder';
import { MainContentContainer } from '../styles/MainContentStyles';

const MainContent = () => {
  return (
    <MainContentContainer>
      <PlaceOrder />
      <Map />
    </MainContentContainer>
  );
};

export default MainContent;
