import styled from 'styled-components';
import map from '../assets/map.png';
import sizes from './sizes';
import colors from './colors';

const MapImage = styled.div`
  flex-basis: 604px;
  background: url(${map});
  margin-left: 15px;
  border-radius: ${sizes.borderRadiusLarge};
`;

const ColorBlend = styled.div`
  display: block;
  height: 100%;
  width: 100%;
  background: ${colors.bgLight};
  mix-blend-mode: color;
`;

export { MapImage, ColorBlend };
