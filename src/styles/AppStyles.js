import styled from 'styled-components';
import colors from './colors';
import sizes from './sizes';

const AppContainer = styled.div`
  background: ${colors.bgDark};
  height: 100%;
`;

const ContentContainer = styled.div`
  height: 100%;
  display: flex;
  margin: 0 ${sizes.margin};
`;

export { AppContainer, ContentContainer };
