import styled from 'styled-components';
import colors from './colors';
import sizes from './sizes';

const MainContentContainer = styled.div`
  flex: 3;
  display: flex;
  padding: 15px;
  background: ${colors.bgLightGray};
  height: 826px;
  border-radius: ${sizes.borderRadiusLarge};
  margin-bottom: 21px;
`;

export { MainContentContainer };
