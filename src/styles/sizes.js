export default {
  base: 4,
  margin: '10px',
  borderRadiusLarge: '20px',
  borderRadiusMedium: '12px',
  borderRadiusSmall: '6px',
};
