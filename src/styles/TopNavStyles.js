import styled from 'styled-components';
import logo from '../assets/logo.png';
import logoText from '../assets/logoText.png';
import sizes from '../styles/sizes';

const TopNavContainer = styled.div`
  display: flex;
  align-items: center;
  height: ${sizes.base * 16}px;
`;

const LogoContainer = styled.div`
  width: 242px;
  display: flex;
  align-items: center;
  padding-left: ${sizes.base * 7}px;
  cursor: pointer;
`;
const Logo = styled.div`
  background-image: url(${logo});
  height: 21px;
  width: 21px;
  margin-right: ${sizes.base * 2}px;
`;
const LogoText = styled.div`
  background-image: url(${logoText});
  height: 9.19px;
  width: 165px;
  filter: invert(1);
`;

const Input = styled.div`
  border: 1px solid #ffffff;
  position: relative;
  margin-left: ${sizes.base * 5}px;
  border-radius: 17px;
  height: 34px;
  width: 34px;
  cursor: pointer;

  &:after {
    content: '•••';
    position: absolute;
    color: white;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%) rotate(90deg);
    letter-spacing: 2px;
  }
`;

export { TopNavContainer, LogoContainer, Logo, LogoText, Input };
