export default {
  textDark: '#202224',
  bgDark: '#1E203E',
  bgLight: '#292B4C',
  bgLightGray: '#F1F3F5',
  info: {
    orange: 'linear-gradient(315deg, #ffb23f 0%, #f97e26 100%)',
    blue: 'linear-gradient(164.58deg, #027AFF -91.84%, #2BDFF3 213.97%)',
    green: 'linear-gradient(315deg, #47C306 0%, #6DD400 100%)',
    purple: 'linear-gradient(133.74deg, #8E57FF 0%, #6D5BFF 102.3%)',
    red: 'linear-gradient(90.66deg, #F24C4C 0.18%, #FF7D45 100%)',
    pink: 'linear-gradient(339.27deg, #FF986D -83.6%, #F03097 148.19%)',
  },
  blue: '#0786FE',
  darkBlueGradient:
    'linear-gradient(143.68deg, #1E3C72 2.38%, #2A5298 116.35%)',
  lightBlueGradient:
    'linear-gradient(303.08deg, #7B94AE -17.13%, #A3BFDB 100%)',
};
