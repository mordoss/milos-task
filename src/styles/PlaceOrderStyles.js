import styled from 'styled-components';
import sizes from './sizes';
import colors from './colors';
import icons from '../assets/detailsIcons';

const PlaceOrderContainer = styled.div`
  flex: 1;
  background: white;
  height: 100%;
  padding: ${sizes.base * 4}px;
  border-radius: ${sizes.borderRadiusLarge};
`;

const InfoContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(3, minmax(0, 1fr));
  grid-template-rows: 78px 78px;
  grid-gap: ${sizes.base * 4}px;
`;
const InfoItem = styled.div`
  background: ${(props) => colors.info[props.color]};
  border-radius: ${sizes.borderRadiusMedium};
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding: ${sizes.base * 3}px 0;
`;

const InfoNumber = styled.p`
  width: 35px;
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: ${sizes.base * 8}px;
  color: white;
  margin: 0;
`;

const InfoText = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  color: white;
  margin: 0;
`;

const DeliveryInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
`;
const DeliveryInfoItem = styled.div`
  display: flex;
  align-items: center;
  background: ${(props) =>
    props.dark ? colors.darkBlueGradient : colors.lightBlueGradient};
  height: 59px;
  margin-top: ${sizes.base * 4}px;
  padding-left: ${sizes.base * 5}px;
  border-radius: ${sizes.borderRadiusMedium};
`;

const InfoDeliveryText = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: 19px;
  color: white;
  margin-left: ${sizes.base * 3}px;
`;

const PlaceOrderActionContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const PlaceOrderTitle = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 26px;
  letter-spacing: -0.0928571px;
  color: ${colors.textDark};
  margin-top: ${sizes.base * 6}px;
  margin-bottom: 0px;
`;

const Details = styled.div`
  display: flex;
  justify-content: space-between;
  border: 1px solid #e1e3e3;
  border-radius: ${sizes.base * 2}px;
  height: 50px;
  margin-top: ${sizes.base * 4}px;
  align-items: center;
  padding: 0 ${sizes.base * 4}px;
`;

const DetailsIcon = styled.div`
  width: ${sizes.base * 6}px;
  height: ${sizes.base * 6}px;
  background: url(${(props) => icons[props.icon]});
  margin: 0 ${sizes.base * 4}px 0 0;
`;
const DetailsText = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: ${sizes.base * 4}px;
  line-height: 19px;
  color: ${colors.textDark};
`;

const DetailsAdd = styled.a`
  font-family: Inter;
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 18px;
  color: ${colors.blue};
  margin-left: auto;
  cursor: pointer;
`;

const DetailsButton = styled.div`
  background: linear-gradient(135deg, #027aff 0%, #2bdff3 100%);
  border-radius: ${sizes.borderRadiusSmall};
  height: ${sizes.base * 11}px;
  text-align: center;
  margin-top: ${sizes.base * 6}px;
`;

const DetailsButtonText = styled.p`
  font-family: Inter;
  font-style: normal;
  font-weight: 500;
  font-size: 13px;
  line-height: ${sizes.base * 4}px;
  color: white;
  cursor: pointer;
`;

export {
  PlaceOrderContainer,
  InfoContainer,
  InfoItem,
  InfoNumber,
  InfoText,
  DeliveryInfoContainer,
  DeliveryInfoItem,
  InfoDeliveryText,
  PlaceOrderActionContainer,
  PlaceOrderTitle,
  Details,
  DetailsIcon,
  DetailsText,
  DetailsAdd,
  DetailsButton,
  DetailsButtonText,
};
