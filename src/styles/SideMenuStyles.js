import styled from 'styled-components';
import colors from './colors';
import sizes from './sizes';
import icons from '../assets/menuIcons';
import avatar from '../assets/avatar.png';

const SideMenuContainer = styled.div`
  flex-basis: 242px;
  display: flex;
  flex-direction: column;
  margin-right: ${sizes.margin};
  background: ${colors.bgLight};
  border-top-left-radius: ${sizes.borderRadiusLarge};
  border-top-right-radius: ${sizes.borderRadiusLarge};
  padding-top: ${sizes.base * 4}px;
  padding-bottom: ${sizes.base * 5}px;
  justify-content: space-between;
`;

const Tabs = styled.div`
  display: flex;
  flex-direction: column;
`;

const Tab = styled.a`
  display: flex;
  flex-basis: 50px;
  align-items: center;
  cursor: pointer;
  color: white;
  border-radius: ${sizes.borderRadiusSmall};
  ${(props) =>
    props.active &&
    `
    background: linear-gradient(90deg, #027AFF 0%, #2BDFF3 100%), #222222;;
  `}
`;

const Icon = styled.div`
  background-image: url(${(props) => icons[props.icon]});
  height: ${sizes.base * 6}px;
  width: ${sizes.base * 6}px;
  margin-left: 18px;
  margin-right: ${(props) => (props.settings ? '8.52px' : '16px')};
`;

const TabText = styled.p`
  font-family: Inter;
  font-size: 14px;
  line-height: 17px;
`;
const Settings = styled.div`
  display: flex;
  cursor: pointer;
  color: white;
  align-items: center;
`;

const Avatar = styled.div`
  background-image: url(${avatar});
  height: ${sizes.base * 8}px;
  width: ${sizes.base * 8}px;
  margin-left: 14px;
  margin-right: -5px;
`;

export { SideMenuContainer, Tabs, Tab, Icon, TabText, Settings, Avatar };
